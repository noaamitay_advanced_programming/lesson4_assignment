#include "Client.h"
#include <cstdlib>
#include <thread>
#include <vector>
#include "hex.h"

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>

CryptoDevice cryptoDevice;

/*
	Input: a message to encrypt
	Output: the message after the encrypt
	The function encrypts the message with the md5 algorithm.
*/
string md5Algorithem(string message)
{
	byte digest[CryptoPP::Weak1::MD5::DIGESTSIZE];

	CryptoPP::Weak::MD5 hash;
	hash.CalculateDigest(digest, (const byte*)message.c_str(), message.length());

	CryptoPP::HexEncoder encoder;
	std::string output;

	encoder.Attach(new CryptoPP::StringSink(output));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();

	return output;
}

/*
	Input: none
	Output: whether the client succeeded to connect or not
	The function connects clients using the users vector
*/
bool connectClients()
{
	string userInput = "";
	int attempts = 3;
	bool rightUser = false;
	vector<string> users;

	//Names and passwords
	users.push_back("Noa LexyBear");
	users.push_back("Keren Dafna");
	users.push_back("Lidor LXTreato");
	users.push_back("Nir MrBarniro");

	cout << "Register secretly!" << endl;

	while (attempts > 0 && !rightUser) //As long as there are still more attempts and the user hasn't connected yet
	{
		getline(cin, userInput); //Get input from user
		if (find(users.begin(), users.end(), userInput) != users.end()) //Check if the user has the right username and password and if so:
		{
			//Seperate between the username and password using the space char
			size_t position = userInput.find(" ") + 1; 
			string username = userInput.substr(0, position);
			string password = userInput.substr(position);

			cout << "name: " << username << endl;
			cout << "password: " << md5Algorithem(password) << endl; //Use the md5 algorithm in order to print the encrypted message
			cout << "Agent accepted. welcome in!" << endl;
			rightUser = true; //The user has connected
		}
		else
		{
			attempts--; //One less attempt
			cout << "wrong password - try again" << endl;
		}
	}

	return rightUser;
}

int process_client(client_type &new_client)
{
	while (1)
	{
		std::memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{


			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

			std::string strMessage(new_client.received_message);

			// some logic, we dont want to decrypt notifications sent by the operator/
			// our protocol says - ": " means notification from the operator
			size_t position = strMessage.find(": ") + 2;
			std::string prefix = strMessage.substr(0, position);
			std::string postfix = strMessage.substr(position);
			std::string decrypted_message;

			//this is the only notification we use right now :(
			if (postfix != "Disconnected") {
				//please decrypt this part!
				decrypted_message = cryptoDevice.decryptAES(postfix);
			}
			else
			{
				//dont decrypt this - not classified and has not been ecrypted! 
				//trying to do so may cause errors
				decrypted_message = postfix;
			}

			if (iResult != SOCKET_ERROR)
			{
				std::cout << prefix + decrypted_message << std::endl;
			}
			else
			{
				std::cout << "recv() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}
		}
	}

	if (::WSAGetLastError() == WSAECONNRESET)
		std::cout << "The server has disconnected" << std::endl;

	return 0;
}

int main()
{
	WSAData wsa_data;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	std::string sent_message = "";
	client_type client = { INVALID_SOCKET, -1, "" };
	int iResult = 0;
	std::string message;

	std::cout << "Starting Client...\n";

	// Initialize Winsock
	iResult = ::WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
		std::cout << "WSAStartup() failed with error: " << iResult << std::endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	std::cout << "Connecting...\n";

	// Resolve the server address and port
	iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		std::cout << "getaddrinfo() failed with error: " << iResult << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (client.socket == INVALID_SOCKET) {
			std::cout << "socket() failed with error: " << ::WSAGetLastError() << std::endl;
			::WSACleanup();
			std::system("pause");
			return 1;
		}

		// Connect to server.
		iResult = ::connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(client.socket);
			client.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (client.socket == INVALID_SOCKET) {
		std::cout << "Unable to connect to server!" << std::endl;
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	bool connected = connectClients();
	if (!connected)
	{
		system("PAUSE");
		return 1;
	}

	std::cout << "Successfully Connected" << std::endl;

	//Obtain id from server for this client;
	::recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
	message = client.received_message;

	if (message != "Server is full")
	{
		client.id = std::atoi(client.received_message);

		std::thread my_thread(process_client, std::ref(client));

		while (1)
		{
			std::getline(std::cin, sent_message);

			//top secret! please encrypt
			std::string cipher = cryptoDevice.encryptAES(sent_message);

			iResult = ::send(client.socket, cipher.c_str(), cipher.length(), 0);

			if (iResult <= 0)
			{
				std::cout << "send() failed: " << ::WSAGetLastError() << std::endl;
				break;
			}


		}

		//Shutdown the connection since no more data will be sent
		my_thread.detach();
	}
	else
		std::cout << client.received_message << std::endl;

	std::cout << "Shutting down socket..." << std::endl;
	iResult = ::shutdown(client.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		std::cout << "shutdown() failed with error: " << ::WSAGetLastError() << std::endl;
		::closesocket(client.socket);
		::WSACleanup();
		std::system("pause");
		return 1;
	}

	::closesocket(client.socket);
	::WSACleanup();
	std::system("pause");
	return 0;
}